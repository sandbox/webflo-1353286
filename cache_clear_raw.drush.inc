<?php
/**
 * @file
 * Clear all caches
 */

/**
 * Implementation of hook_drush_command().
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing each command.
 */
function cache_clear_raw_drush_command() {
  $items = array();

  $items['cache-clear-raw'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
    'description' => "Clear cache",
    'options' => array(
      'exclude' => 'Exclude tables',
    ),
    'examples' => array(
      'drush cache-clear-raw --exclude="cache_frm cache_views"',
    ),
    'aliases' => array('ccr'),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function cache_clear_raw_drush_help($section) {
  switch ($section) {
    case 'drush:cache_clear_raw':
      return dt("Clear cache raw");
  }
}

/**
 * Clear cache raw command callback.
 */
function drush_cache_clear_raw() {
  $exclude = array(
    'cache_form',
  );

  $options = drush_get_option('exclude');
  if (isset($options)) {
    $exclude_tables = explode(',', str_replace(' ', '', $options));
    $exclude = $exclude + $exclude_tables;
  }

  $exclude = _drush_cache_clear_map_assoc($exclude);
  $result = db_query("SHOW TABLES LIKE 'cache_%'");
  while ($table_name = drush_db_result($result)) {
    if (!isset($exclude[$table_name])) {
      db_query("TRUNCATE TABLE {$table_name}");
    }
  }
}

function _drush_cache_clear_map_assoc($array, $function = NULL) {
  // array_combine() fails with empty arrays:
  // http://bugs.php.net/bug.php?id=34857.
  $array = !empty($array) ? array_combine($array, $array) : array();
  if (is_callable($function)) {
    $array = array_map($function, $array);
  }
  return $array;
}
